const { Router } = require('express');
const router = Router();
const BooksRoutes = require('./books')


router.get('/', (req,res)=>{
    // res.render('index.ejs')
});
router.use('/books', BooksRoutes)

// router.get('/lecturers', (req, res) => {
//     res.send('Ini page lecturers')
// })

module.exports = router;
