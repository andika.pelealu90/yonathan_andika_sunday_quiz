const { Router } = require('express');
const router = Router();
const BooksController = require('../controller/booksController')


//get method
router.get('/',BooksController.getBooks)
router.get('/books/add',BooksController.addBooksForm)
router.get('/books/edit/:id',BooksController.editBooksForm)
router.get('/books/delete/:id',BooksController.deleteBooks)
//post method
router.post('/books/add',BooksController.addBooks)
router.post('/books/edit:id',BooksController.editBooks)


module.exports = router