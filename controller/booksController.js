const { Books } = require('../models')

class BooksController {
    static getBooks(req,res) {
        Books.findAll()
            .then(result =>{
                res.render('books.ejs', {books : result})
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addBooks(req, res) {
        const { title, author,release_date, pages,genre} = req.body
    Books.create({
        title, 
        author, 
        release_date, 
        pages
    })
        .then(result => {
            res.redirect('/books')
        })
        .catch(err => {
            console.log(err);
        })
    }
    static addBooksForm(req, res) {
        res.render('addBooks.ejs');
    }
    static editBooksForm(req, res) {
        res.render('editBooks.ejs');
    }
    static editBooks(req, res) {
        const id = req.params.id;
        const { title, author,release_date, pages} = req.body;
        Student.update({
            title,
            author,
            release_date,
            pages
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
    static deleteBooks(req, res) {
        const id = req.params.id;
        Books.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/books')
            })
            .catch(err => {
                res.send(err)
            })
    }


}

module.exports = BooksController;