'use strict';
const fs = require('fs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   title: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const parseData = JSON.parse(fs.readFileSync('./books.json'));
   const BooksData = [];
   parseData.forEach(data => {
     const { title, author, release_date, pages,genre } = data;
     BooksData.push({
      title, 
      author,
      release_date,
      pages,
      genre,
       createdAt : new Date(),
       updatedAt : new Date()
     })
   })
   await queryInterface.bulkInsert('Books', BooksData, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Books', null, {});
  }
};
